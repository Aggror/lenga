function Script:Start()
	--Create a water material
	self.mat = Material:Create()
	--self.shader = Shader:Load("Shaders/Dynamic/refract+specular.shader") -- Projection shader
	self.mat:SetTexture(Texture:Load("Materials/stone_dot3.tex"),1) -- normal map for refraction
	self.mat:SetShader(self.shader)

	--Paint me
	self.entity:SetMaterial(self.mat);

	--create the buffer for color and depth
	self.buffer = Buffer:Create(256,256,1,0,0)
	--And a texture for it.
	self.buffer:SetColorTexture(Texture:Create(256,256))
end

function Script:Draw()
	--Clear and enble the buffer
	self.buffer:Clear()
	self.buffer:Enable()

	--Hide water enity
	self.entity:Hide()
	--Render to buffer
	App.world:Render()
	self.buffer:Disable()

	--Apply buffer as material
	self.mat:SetTexture(self.buffer:GetColorTexture(),0) --get buffer for diffuse
	self.mat:SetTexture(self.buffer:GetDepthTexture(),3) --get scene depth

	--Switch back to context and show water
	Context:SetCurrent(App.context);
	self.entity:Show()
end