--Move the mouse to the center of the screen
centerMouse = Vec2(App.context:GetWidth()/2, App.context:GetHeight()/2 )
App.window:SetMousePosition(centerMouse.x, centerMouse.y)
mouseSensitivity = 15
mouseDifference  = Vec2(0,0)
camRotation      = Vec3(0,0,0)

--Create camera pivot
fpsPivot = Pivot:Create()
tpsPivot = Pivot:Create()

--Set some variables
cameraTopAngle = 2
cameraBottomAngle = 60
camSmoothing = 8.0
lowerSpeed = 0.2
rotSpeed = 1.2

--Tps camera
maxCamOffset = -8.0
minCamOffset = 1.5

function Script:Start()
	fpsPivot:SetPosition(self.entity:GetPosition())	
end

function Script:UpdateWorld()

	local camHeight = ((App.window:KeyDown(Key.W) and 1 or 0) - (App.window:KeyDown(Key.S)and 1 or 0)) * Time:GetSpeed() * lowerSpeed
	local rotaY = ((App.window:KeyDown(Key.D) and 1 or 0) - (App.window:KeyDown(Key.A)and 1 or 0)) * Time:GetSpeed() * rotSpeed
	
	 --Get the mouse movement
        local currentMousePos = App.window:GetMousePosition()
        mouseDifference.x = currentMousePos.x - centerMouse.x
        mouseDifference.y = currentMousePos.y - centerMouse.y

        --Adjust and set the camera rotation
        --local tempX = camRotation.x + (mouseDifference.y / mouseSensitivity)
        --if tempX > cameraTopAngle and tempX < cameraBottomAngle then
        --        camRotation.x = tempX
        -- end
        camRotation.y = camRotation.y + (mouseDifference.x / mouseSensitivity)
	camRotation.y = camRotation.y + rotaY

        fpsPivot:SetRotation(camRotation.x,camRotation.y * -1,camRotation.z)
        App.window:SetMousePosition(centerMouse.x, centerMouse.y)

        --Store player some information
        local tempFpsPos = fpsPivot:GetPosition()
        fpsPivot:SetPosition(tempFpsPos)

        --Position and Rotate the camera to FPS pivot
        App.camera:SetPosition(fpsPivot:GetPosition())
        App.camera:SetRotation(fpsPivot:GetRotation())

        --Calculate the furthest TPS pivot position
        tpsPivot:SetPosition(fpsPivot:GetPosition())
        tpsPivot:SetRotation(fpsPivot:GetRotation())
        tpsPivot:Move(0, 0, maxCamOffset, false)
       
	local newFPSPos = fpsPivot:GetPosition()
	if newFPSPos.y + camHeight > 0 then
		fpsPivot:SetPosition( newFPSPos.x, newFPSPos.y + camHeight, newFPSPos.z)
	end
	
	 App.camera:SetPosition(tpsPivot:GetPosition())
end