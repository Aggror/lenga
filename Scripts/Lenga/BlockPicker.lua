Script.pickID = 0
Script.curColor = 1

function Script:UpdateWorld()
	if gameOver == false then
		local pickinfo = PickInfo()
		local p = App.window:GetMousePosition() 
		if (App.camera:Pick(p.x,p.y,pickinfo,0,true)) then	

			--remove on click
			if App.window:MouseHit(Key.LButton) then
				self.pickID = pickinfo.entity:GetKeyValue("id")
				self.colorID = pickinfo.entity:GetKeyValue("colorid")
				
				if tonumber(self.colorID) == self.curColor then
					--make sure not the top row of blox
					local canRemove = true
					for i = totalBlocks - (widthCount-1), totalBlocks , 1 do
						if tonumber(self.pickID) == i then
							canRemove = false
							break
						end
					end

					--If can remove is true, then we remove 
					if canRemove then
						self.component:CallOutputs("BlockRemoval")
						--pick random new number
						self.curColor = math.random(1, colorCount)
					end
				end
			end
		end
	end
end

function Script:GetPickID()--arg
	return self.pickID
end

function Script:PostRender()
	App.context:SetBlendMode(Blend.Alpha)
	App.context:SetColor(1,1,1)
	App.context:DrawText("Remove block with color: ", 0,27)

	if self.curColor == 1 then
		App.context:SetColor(1,0,0)
	elseif self.curColor == 2 then
		App.context:SetColor(0,0,1)
	elseif self.curColor == 3 then
		App.context:SetColor(0,1,0)
	end
	App.context:DrawRect(70,55,42,42)

	App.context:SetColor(0.9,0.8,0.9)
	App.context:DrawRect(App.context:GetWidth()/2-8,App.context:GetHeight()/2-8,12,12)
	App.context:SetBlendMode(Blend.Solid)	
end