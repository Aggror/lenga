gameOver = false

function Script:Start()
	
end

function Script:UpdateWorld()

end

function Script:PostRender()
	if gameOver then
		App.context:SetBlendMode(Blend.Alpha)
		App.context:SetColor(1,1,1)
		App.context:DrawText("GAME OVER", 440,400)
		App.context:DrawText("Press R to restart", 425,430)
		App.context:SetBlendMode(Blend.Solid)	
	end
end

function Script:Collision(entity, position, normal, speed)
	gameOver = true
	
end