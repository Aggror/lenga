Script.bloxInHeight = 10 --int "Blocks in height"
Script.bloxInWidth = 4 --int "Blocks in Width"
Script.colorAmount = 0 -- int "Color amount"
Script.bloxToDestroy = 10 --int "Blox2destroy"
Script.bloxDestroyCounter = 0
tower = nil
Script.evenLayer = true
Script.type = "box"
colorCount = 0
totalBlocks = 0
widthCount = 0
levelCompleted = false
Script.mat = ""

function Script:Start()
	tower = {}
	self.colorCounter = {}
	evenLayer = true
	colorCount = self.colorAmount
	widthCount = self.bloxInWidth
	totalBlocks = self.bloxInHeight * self.bloxInWidth
	self.bloxDestroyCounter = 0
	self.mat = Material:Load("Materials/border.mat")

	--Create a box for copying
	local box = Model:Box()
	box:SetPosition(0,20,0)
	box:SetMaterial(self.mat)

	local boxShape = Shape:Box(0,0,0, 0,0,0, 1,1,1)
	box:SetShape(boxShape)
	box:SetCollisionType(Collision.Prop) 

	for i = 0, self.bloxInHeight-1, 1 do
		for j = 0, self.bloxInWidth-1, 1 do
			local boxCopy = box:Copy()
				
			--scale	
			if evenLayer then
				boxCopy:SetScale(1,1,self.bloxInWidth)
			else
				boxCopy:SetScale(self.bloxInWidth,1,1)
			end

			--Position and rotation
			if evenLayer then
				boxCopy:SetPosition(j * 1,i,0)
			else
				boxCopy:SetPosition((self.bloxInWidth/2) - 0.5,i, (j * 1) -(self.bloxInWidth/2) + 0.5)
			end
			
			--Set color
			local colorid = math.random(1, self.colorAmount)
			System:Print(colorid)	
			if colorid == 1 then	
				boxCopy:SetKeyValue("colorid", colorid)
				boxCopy:SetColor(1,0,0)

			elseif colorid == 2 then	
				boxCopy:SetKeyValue("colorid", colorid)
				boxCopy:SetColor(0,0,1)

			elseif colorid == 3 then	
				boxCopy:SetKeyValue("colorid", colorid)
				boxCopy:SetColor(0,1,0)
			end

			local index = (i*self.bloxInWidth)+ j + 1
			boxCopy:SetKeyValue("id", index)
			tower[index] = boxCopy
		end
		evenLayer = not evenLayer
	end

	--Now that everything is in place we apply mass
	for i = 0, self.bloxInHeight-1, 1 do
		for j = 0, self.bloxInWidth-1, 1 do
			local index = (i*self.bloxInWidth)+ j + 1	
			--only apply mass on boxes are the first row
			if index > self.bloxInWidth then
				tower[index]:SetMass(15)
			end
		end
	end
end

function Script:BlockRemoval(id)--in
	if(tower[tonumber(id)] == nil)then
		System:Print("Block is already removed")
	else
		tower[tonumber(id)]:Release()
		tower[tonumber(id)] = nil
		--decrease blox destroy counter
		self.bloxDestroyCounter = self.bloxDestroyCounter + 1
		if self.bloxToDestroy - self.bloxDestroyCounter < 1 then
			levelCompleted = true
		end
	end
end



function Script:PostRender()
	App.context:SetBlendMode(Blend.Alpha)
	App.context:SetColor(1,1,1)
	if levelCompleted == false then	
		App.context:DrawText("Blox to destroy: " .. tonumber(self.bloxToDestroy - self.bloxDestroyCounter), 700,27)
	else
		App.context:DrawText("You won!!", 750,27)
		App.context:DrawText("Press SPACE for next level!!", 750,85)
	end
	
	App.context:SetBlendMode(Blend.Solid)
end

--[[
function Script:Collision(entity, position, normal, speed)
	
end
]]--

--[[
function Script:DrawEach(camera)
	
end
]]--

--[[
function Script:Release()
	
end
]]--

--[[
function Script:Cleanup()
	
end
]]--