--This function will be called once when the program starts
function App:Start()
	
	self.info = false
	--Set the application title
	self.title="Lenga"
	
	--Create a window
	self.window=Window:Create(self.title)
	self.window:HideMouse()
	
	--Create the graphics context
	self.context=Context:Create(self.window,0)
	if self.context==nil then return false end
	
	--Create a world
	self.world=World:Create()
	self.camera = Camera:Create()
	
	--Load a map
	local mapfile = System:GetProperty("map","Maps/start.map")
	if Map:Load(mapfile)==false then return false end
	
	return true
end

--This is our main program loop and will be called continuously until the program ends
function App:Loop()
	
	--If window has been closed, end the program
	if self.window:Closed() or self.window:KeyDown(Key.Escape) then return false end
	
	if self.window:KeyHit(Key.Space) and levelCompleted then
		self.world:Clear()
		self.camera = Camera:Create()
		if Map:Load("Maps/level2.map")==false then return false end
	end

	if self.window:KeyHit(Key.R) and gameOver then
		self.world:Clear()
		self.camera = Camera:Create()
		if Map:Load("Maps/start.map")==false then return false end
	end

	if self.window:KeyHit(Key.I)then
		self.info = not self.info
	end



	--Update the app timing
	Time:Update()
	
	--Update the world
	self.world:Update()
	
	--Render the world
	self.world:Render()
	
	if self.info then
		App.context:SetBlendMode(Blend.Alpha)
		App.context:SetColor(0,0,1)
		App.context:DrawRect(0,0,1024,150)
		App.context:SetColor(1,1,1)
		App.context:DrawText("Goal of the game: Remove the amount of blox from the towwer, without making it tumble!", 3, 25)
		App.context:DrawText("Use A and D to rotate around the camera.", 3, 50)
		App.context:DrawText("Use W and S to move the camera up and down.", 3, 75)
		App.context:DrawText("Left click to remove the block that the white indicator is pointing at.", 3,100)
		App.context:DrawText("Press i for info", 3, 125)
		App.context:DrawText("LENGA -- Made by Jorn aka Aggror.", 650, 130)
		App.context:SetBlendMode(Blend.Solid)		
	else
		App.context:SetBlendMode(Blend.Alpha)
		App.context:SetColor(1,1,1)
		App.context:DrawText("Press i for info", 3, 3)
		App.context:SetBlendMode(Blend.Solid)
	end	

	--Refresh the screen
	self.context:Sync()
	
	--Returning true tells the main program to keep looping
	return true
end